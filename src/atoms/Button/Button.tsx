import {FC} from "react";
import styles from './Button.module.scss';

interface ButtonProps {
    text: string;
    type: 'submit';
}

const Button:FC<ButtonProps> = ({text, type}) => {
    return (
        <button type={type} className={styles.element}>{text}</button>
    )
}

export default Button;