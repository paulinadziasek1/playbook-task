import {FC} from "react";
import {IColumns} from "../../constans";
import {ExpensesStoreImpl, IExpensesList} from "../../store/ExpensesStore";
import styles from './TableBody.module.scss';
import Paragraph from "../Typography/Paragraph/Paragraph";

interface TableBodyProps {
    columns: IColumns[];
    tableData: IExpensesList[];
    customLabelBodyName?: string;
    expensesStore: ExpensesStoreImpl;
}

const TableBody:FC<TableBodyProps> = ({
    expensesStore,
    columns,
    tableData,
    customLabelBodyName}) => {

    return (
        <div>
            {tableData.map((data) => {
                return (
                    <div className={styles.wrapper} key={data.id}>
                        {columns.map(({ accessor }) => {
                            const tData = data[accessor as keyof IExpensesList] ? data[accessor as keyof IExpensesList] : "——";
                            return <Paragraph text={tData!} key={accessor} />
                        })}
                        { customLabelBodyName && <Paragraph
                            onClick={() => expensesStore.deleteExpenses(tableData, data.id!)}
                            className={styles.button}
                            text={customLabelBodyName}
                        />}
                    </div>
                );
            })}
        </div>
    )
}

export default TableBody;