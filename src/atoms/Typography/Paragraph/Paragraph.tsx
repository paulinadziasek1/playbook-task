import {FC} from "react";

interface ParagraphProps {
    text: string | number;
    className?: string;
    onClick?: () => void;
}

const Paragraph:FC<ParagraphProps> = ({ text, className, onClick }) => {
    return (
        <p onClick={onClick} className={className}>{text}</p>
    )
}

export default Paragraph;