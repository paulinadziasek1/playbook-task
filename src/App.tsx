import React, {FC} from 'react';
import styles from './App.module.scss';
import Expenses from "./components/Expenses/Expenses";
import {ExpensesStore} from "./store/ExpensesStore";

const App: FC = () => {

    return (
      <div className={styles.wrapper}>
          <Expenses expensesStore={ExpensesStore}/>
      </div>
  )
}

export default App;
