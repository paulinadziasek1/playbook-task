import React from "react";
import styles from "./InputError.module.scss";
import cn from "classnames";

const InputError = ({ errors, inputName, trade }: any) => {
    return (
        <span className={cn(styles.element, trade && styles["isTrading"], "d-inline-flex align-items-center")}>
            {errors[inputName]}
        </span>
    );
};
export default InputError;
