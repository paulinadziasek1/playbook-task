import {FC} from "react";
import styles from './Heading.module.scss';
import cn from "classnames";

interface HeadingProps {
    text: string;
    className?: string;
}

const Heading:FC<HeadingProps> = ({ text, className }) => {
    return (
        <div className={cn(styles.element, className)}>{text}</div>
    )
}

export default Heading;