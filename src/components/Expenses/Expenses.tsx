import {FC, useState} from "react";
import {Field, Form, Formik} from "formik";
import Heading from "../../atoms/Typography/Heading/Heading";
import styles from './Expenses.module.scss';
import TextInput from "../../atoms/Form/TextInput/TextInput";
import Button from "../../atoms/Button/Button";
import Paragraph from "../../atoms/Typography/Paragraph/Paragraph";
import Table from "../../atoms/Table/Table";
import {ExpensesStoreImpl} from "../../store/ExpensesStore";
import {observer} from "mobx-react";
import {replaceCommaOnDot} from "../../helpers";
import {useExpensesValidationSchema} from "../../validation/expenses";

interface ExpensesProps {
    expensesStore: ExpensesStoreImpl
}

const columns = [
    { label: "Title", accessor: "title" },
    { label: "Amount(PLN)", accessor: "amountPln" },
    { label: "Amount(EUR)", accessor: "amountEur" },
];

const Expenses:FC<ExpensesProps> = observer(({expensesStore}) => {
    const {expensesList} = expensesStore;
    const expensesValidationSchema = useExpensesValidationSchema();
    const [valueEuro] = useState('4,382');

    const amountPln = expensesList.map(({amountPln}) => amountPln);
    const sumAmountPln = parseFloat(replaceCommaOnDot(amountPln.reduce((a, b) => a + b, 0).toFixed(4)));

    const parsedData = expensesList
        .map((data) => ({
            ...data,
            amountPln: data.amountPln,
            amountEur: (data.amountPln / parseFloat(replaceCommaOnDot(valueEuro))).toFixed(2),
            id: Math.floor(Math.random() * 100)
    }))

    return (
        <div className={styles.element}>
            <Formik
                initialValues={{
                    title: '',
                    amountEur: valueEuro,
                    amountPln: '',
                }}
                onSubmit={(values,{resetForm}) => {
                    const parseSendingData = {
                        ...values,
                        amountPln: parseFloat(replaceCommaOnDot(values.amountPln)),
                        amountEur: parseFloat(replaceCommaOnDot(values.amountEur)),
                    }
                    expensesStore.sendExpenses(parseSendingData)
                    resetForm();
                }}
                validationSchema={expensesValidationSchema}
            >
                {({
                      handleChange,
                      handleBlur,
                      values,
                      handleSubmit }) => (
                    <Form className={styles.form} onSubmit={handleSubmit}>
                        <div className={styles.headingBox}>
                            <Heading className={styles.heading} text={'List of expenses'} />
                            <Field
                                label={'Value Euro'}
                                name={'amountEur'}
                                className={styles.input}
                                value={values.amountEur}
                                component={TextInput}
                            />
                        </div>
                        <Field
                            label={'Title of transation'}
                            name="title"
                            className={styles.input}
                            value={values.title}
                            pattern="[A-Za-z]{5,}"
                            component={TextInput}
                        />
                        <div className={styles.headingBox}>
                            <Field
                                label={'Amount in PLN'}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                name={'amountPln'}
                                className={''}
                                value={values.amountPln}
                                component={TextInput}
                                pattern="^[0-9]*[\.|,]?[0-9]{0,2}$"
                            />
                            <Button type="submit" text={'Add'} />
                        </div>
                    </Form>
                )}
            </Formik>
            {parsedData.length &&
                <>
                    <Table
                        customLabelName={'Options'}
                        customLabelBodyName={'Delete'}
                        columns={columns}
                        tableData={parsedData}
                        expensesStore={expensesStore}
                    />
                    <Paragraph
                        className={styles.sum}
                        text={`Sum: ${sumAmountPln} PLN (${(sumAmountPln / parseFloat(replaceCommaOnDot(valueEuro))).toFixed(2) } EUR)`}
                    />
                </>
            }
        </div>
    )
});

export default Expenses;