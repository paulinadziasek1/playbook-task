import { FC } from "react";
import cn from "classnames";
import { FieldInputProps } from "formik";
import ErrorInput from "../InputError/InputError";
import Label from "../../Label/Label";
import styles from "./TextInput.module.scss";

interface IconFormProps {
    label: string;
    field: FieldInputProps<any> | any;
    form: any;
    className?: string;
    value: string;
    pattern?: string;
}

const TextInput: FC<IconFormProps> = ({ label, field, form, className, value,pattern}) => {
    return (
        <div className={cn(styles.wrapper, className)}>
            {label && <div className={styles.label}><Label>{label}</Label></div>}
            <div>
                <input
                    value={value}
                    name={field.name}
                    onChange={field.onChange}
                    type='text'
                    pattern={pattern}
                />
            </div>
            {form.touched[field.name] && form.errors[field.name] && (
                <ErrorInput errors={form.errors} inputName={field.name} />
            )}
        </div>
    );
};

export default TextInput;
