import {FC} from "react";
import TableHead from "../TableHead/TableHead";
import TableBody from "../TableBody/TableBody";
import {IColumns} from "../../constans";
import styles from './Table.module.scss';
import {ExpensesStoreImpl} from "../../store/ExpensesStore";


export interface TableProps {
    columns: IColumns[];
    tableData: Array<any>;
    customLabelName?: string
    customLabelBodyName?: string;
    expensesStore: ExpensesStoreImpl;
}

const Table:FC<TableProps> = ({expensesStore, columns, tableData, customLabelName, customLabelBodyName}) => {

    return (
        <div className={styles.element}>
            <TableHead
                customLabelName={customLabelName}
                columns={columns}
            />
            <TableBody
                expensesStore={expensesStore}
                customLabelBodyName={customLabelBodyName}
                columns={columns}
                tableData={tableData}
            />
        </div>
    )
}

export default Table;