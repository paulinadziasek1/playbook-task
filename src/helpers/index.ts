export const replaceCommaOnDot = (text: string) => {
    return text.toString().replace(/,/g, '.')
};