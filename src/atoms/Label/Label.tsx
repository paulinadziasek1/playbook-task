import { FC } from "react";
import styles from "./Label.module.scss";
import cn from "classnames";

interface LabelProps {
    children: string;
    className?: string;
    color?: 1 | 2;
}

const Label: FC<LabelProps> = ({ children, className, color = 1 }) => (
    <span className={cn(styles.label, styles[`hasColor-${color}`], className)}>{children}</span>
);

export default Label;
