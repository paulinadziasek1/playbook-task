import {makeAutoObservable} from "mobx";

export interface IExpensesList {
    title: string;
    amountEur: number;
    amountPln: number;
    id?: number;
}

export class ExpensesStoreImpl {
    private __expensesList: IExpensesList[] = [];

    constructor() {
        makeAutoObservable(this)
    }

    public get expensesList() {
        return this.__expensesList;
    }

    public sendExpenses(list: IExpensesList) {
        this.__expensesList = [...this.expensesList, list];
    }

    public deleteExpenses(expensesList: IExpensesList[], payload: number) {
        this.__expensesList = expensesList.filter(({id}) => id !== payload);
    }
}

export const ExpensesStore = new ExpensesStoreImpl()