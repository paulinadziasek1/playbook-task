export interface IColumns {
    label: string;
    accessor: string;
}
