import * as Yup from "yup";
import * as yup from "yup";

export const useExpensesValidationSchema = () => {

    return yup.object().shape({
        title: Yup.string().required('Title is required'),
        amountPln: Yup.string().required('Amount Pln is required'),
    });

};

