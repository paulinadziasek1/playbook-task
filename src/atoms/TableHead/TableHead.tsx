import {FC} from "react";
import {IColumns} from "../../constans";
import styles from './TableHead.module.scss';
import Paragraph from "../Typography/Paragraph/Paragraph";

interface TableHeadProps {
    columns: IColumns[];
    customLabelName?: string;
}

const TableHead:FC<TableHeadProps> = ({columns, customLabelName}) => {
    return (
        <div className={styles.element}>
            {columns.map(({ label, accessor }) => {
                return <Paragraph text={label} key={accessor} />
            })}
            {customLabelName && <Paragraph text={customLabelName} />}
        </div>
    )
}

export default TableHead;